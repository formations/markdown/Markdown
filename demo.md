
# Text formatting

- Italic is obtained by preffixing and suffixing with _ or * (just don't mix them up): either *italic* or _italic_, no *italic_

- **Bold** or __bold__

- ++Underlined++ (doesn't work in all Markdown flavors)

- ~~strikethrough~~


# Lists

## Non numbered lists

- First item
- Second item

or 

* First item
* Second item
    * First sub-item

Both may be mixed:

* First item
* Second item
    * First sub-item

or even (not advised!):

* First item
- Second item
    * First sub-item


## Numbered lists

The numbering is rather clever:

1. First numbered item
1. Second numbered item


--- 

# Links

It is easy to [insert links](https://gitlab.inria.fr/formations/markdown/Markdown) that might be internal (in this case relative paths are used) or external.

Images may be inserted as well with same syntax.




---

# Arrays

| Column 1          |   Column 2    |                    Column 3 |
|:----------------- |:-------------:| ---------------------------:|
| Left-aligned Text | Centered text |          Right-aligned text |
| Lorem ipsum dolor |   sit amet    | consectetur adipiscing elit |

Please note here that it is nice to be able to render an array, but  it is not very practical to use it for instance for collaborative filling of an array.


--- 

# Quotes

> You may quote by this syntax.


--- 

# Footnotes

It is possible to use footnotes[^this_way]

[^this_way]: Insert here the text displayed in the footnote.

The same footnote may be reused several times [^this_way]; in which case in the footnote itself there is the possibility to go back to either location it was called.

There is an alternate syntax with numbers instead of tags[^1]. I advise to use mainly the syntax with tag, but some Markdown flavor may only support the one with numbers so it's good to know it exists.

[^1]: Footnote with a number.

---

# Table of contents

Usually just typing:

[TOC]

generates an up-to-date table of contents (usually of course it is better to put this at the beginning of the document.)

At least on notes.inria.fr (but not in VSCode or Gitlab wiki), you may control the level of details with `maxLevel=`:

[TOC maxLevel=2]

---

# When Markdown is not enough...

There is in most Markdown instances ad hoc support for other tools that enable different renderings.

## html

Markdown recognizes the html syntax, so you may use it for stuff not supported out of the box in Markdown:

### Comments


This [StackOverflow discussion](https://stackoverflow.com/questions/4823468/comments-in-markdown) recommends to use this for comments in Markdown files:

<!---
your comment goes here
and here
-->

Markdown doesn't support text coloring, but you may use as well html syntax:

<p style="color:blue">This text should be in blue (if your Markdown flavor support it).</p>

--- 

## LaTeX


LaTeX is also supported as code in some Markdown editors (ok for instance in mattermost.inria.fr or Gitlab wiki, but not in notes.inria.fr):

```latex
\int_{a}^{b} x^2 dx
```

But not in notes.inria.fr, where the following syntax works: $\int_{a}^{b} x^2 dx$ (but the same isn't activated on Inria Mattermost instance...)


--- 

## Code

### C++ code

```c++
#include <cstdlib>
#include <iostream>

int main(int argc, char** argv)
{
    std::cout << "Hello world" << std::endl;
    exit(EXIT_SUCCESS);
}
```

### Python code

```python
import sys

print("Hello world")
sys.exit()
```
