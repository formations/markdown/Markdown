[TOC]

# Markdown

## What is Markdown?

Markdown is a very simple language to format properly plain-text, with an unobstrusive syntax (contrary to html[^1] or LaTeX[^2] for instance)

[^1]: See [this file](./example.html) for an example to render current paragraph with html.
[^2]: See [this file](./example.tex) for an example to render current paragraph with LaTeX.

It is widely used in tools used by developers

- README of [Gitlab](https://gitlab.inria.fr/solverstack/chameleon) or [GitHub](https://github.com/GUDHI/gudhi-devel) projects
- In Jupyter notebooks

but is in fact present in many tools used far beyond the circle of developers in Inria:

- [notes.inria.fr](https://notes.inria.fr)
- [Mattermost](https://mattermost.inria.fr)
- [Wikis](https://gitlab.inria.fr/dgd-i/seminairestransverses/-/wikis/S%C3%A9minaires-transverses)

It can also be used locally in most text editors (such as VisualStudio code) - files with **md** extension are interpreted as Markdown files.

## Why is it popular?

- As it is simple text, it is very easy to use along a version manager, contrary to for instance a Word document which is a binary. This means much less format incompatibility - you may read it easily with tools installed by default on most OS, and even without the rendering it remains comprehensible.
- Very lightweight documents.
- Very easy learning curve.
- Easy integration of more sophisticated tools such as LaTeX.

## Caveat

There are actually several flavors of Markdown, so some commands might not work everywhere. You may look at the [Wikipedia page](https://en.wikipedia.org/wiki/Markdown) if you want more about the historic and standardization of Markdown or [this site](https://www.markdownguide.org/extended-syntax/) if you want an in-depth analysis of several different flavors and what each of them entails.

## An overview of the commands

### In-built help in tools

Most tools with Markdown offer in-built guides to remind the most essential commands.

- In Mattermost it is by choosing _Help_ > _Formater le texte à l'aide de Markdown_
- In [notes.inria.fr](https://notes.inria.fr) it is by clicking on the help menu when you're editing a note (**not** on the homepage).
- For Gitlab, [this documentation](https://docs.gitlab.com/ee/user/markdown.html)

### Demonstration

In [notes.inria.fr](https://notes.inria.fr), using the content of [this file](./demo.md).


# Conversion to other formats with Pandoc

It is possible to export the generated document in other formats. In [notes.inria.fr](https://notes.inria.fr), it is done directly with the few choices given in the _Menu_ (we'll see that [shortly](#tips-for-notesinriafr))

If you're working locally, you can use [pandoc](https://pandoc.org/) to convert into almost any format you wish. Pandoc is [easy to install](https://pandoc.org/installing.html) on most OS (I didn't try on Windows but it seems covered).

For instance the command: 

````
pandoc README.md -o Markdown.docx
````

provides an acceptable Word version of the present file, or 

````
pandoc README.md -o Markdown.pdf 
````

provides a pdf version.

## Table of contents

Pandoc may also be used to add a table of contents to a Markdown document:

````
pandoc -s --toc --wrap=preserve README.md -o README_with_toc.md
````

You may specify the same name as output file but I advise not to (unless it is tracked by a source manager): pandoc may change your rendering and it might not suit you...
(the `--wrap=preserve` is there to limit this effect at maximum).


# Tips for notes.inria.fr

We will now have a deeper look at notes.inria.fr, covering some tips that aren't necessarily related directly to Markdown.

## Why notes.inria.fr and no longer pad.inria.fr?

### Pros 

- Markdown support and therefore clean and comfortable rendering
- [Cohesive view of all notes](https://notes.inria.fr/) (both created by the user or shared with him/her)
- Easy export into other formats (docx, pdf)
- Efficient collaborative work (no risk to lose data contrary to Mybox/OnlyOffice)
- Backup every 10 minutes

### Cons

- No password option 
- No real-time backup as was present in pad.inria.fr
- Notes not modified for a year are supposed to be removed (see [DSI documentation](https://doc-si.inria.fr/display/SU/Editeur+Collaboratif+NOTES#expand-Combiendetempslesnotessontellesconserves))

## Converting into other formats

You may choose in _Menu_ > _Download_ to download the file in several output formats. In you choose _Pandoc_ you will have several further possibilities provided (including LaTeX, Word or Openoffice formats).

## Setting header of the notes document

At the beginning of your document, you may write a header such as:

````
---
type: document
title: Markdown tutorial
description: Notes used in Markdown tutorial
tags: markdown, demandez_le_programme, notes
---
````

By doing so:

- In notes.inria.fr homepage, the notes will be displayed with the chosen title.
- Tags may be used when using the search engine.

## Specific blocks - info, warnings, etc...

You may use specific blocks to highlight segments of your text:

:::success
Yes :tada:
:::

:::info
This is a message :mega:
:::

:::warning
Watch out :zap:
:::

:::danger
Oh No! :fire:
:::

:::spoiler Click to show details
You found me :stuck_out_tongue_winking_eye:
:::

## Named contribution

_When using a quote_, you may add the name of the person doing the comments with the following syntax:

> [name=Sébastien] Named comment here!


## More...

You may find many more functionalities (how to do flowcharts for instance) by perusing the official documentation [here](https://notes.inria.fr/features) (or by clicking on the _Help_ icon and clicking on _Features_)





